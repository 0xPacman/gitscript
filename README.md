
<p align="center">
  <img src="https://github.com/0xPacman/GITscript/blob/main/gitscript.png" width="80%" height="80%" alt="GITscript logo">
</p>

"if you want it, GIT it".

# GITscript

Usage: 

`sh -c "$(curl -fsSL https://gitlab.com/0xPacman/gitscript/-/raw/main/gitscript.sh)"`

there's three options in the script: 
- Automatic push every N seconds with random commit message
- Normal push with specific commit message (you just need to select files to track and entre the commit message and your code will be pushed)
- Push whenever you change something in the code (select the files that you want to track, change something in the code and it will be pushed)

Features: 

- If you want to see the files that you want to track just type `/list` and a tree of files will show up in order to make things easy for you.
- If you have any suggestion please request it as a feature [here](https://github.com/0xPacman/GITscript/issues/new/choose)
